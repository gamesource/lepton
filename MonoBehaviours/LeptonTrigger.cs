﻿using System.Collections;
using UnityEngine;

class LeptonTrigger : Trigger {
    public bool Triggered;
    //on start
    public void Start()
    {
        Register();
        //start update process on each fixed update
        StartCoroutine(checkCycle());
    }

    IEnumerator checkCycle()
    {
        //forever
        for (;;)
        {
            //on each fixed update
            yield return new WaitForFixedUpdate();
            //monitor achievement observer status
            Triggered = achievementObserver.status;
            //persist status, fire and exit
            if (Triggered)
            {
                //if handler is attached, call it
                if (Unlock != null)
                    //pass achievement id from our parent achievement observer
                    Unlock(achievementObserver.achievementId);
                break;
            }
        }
    }
}
