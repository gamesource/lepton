﻿using UnityEngine;
using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;

public class AchievementObserver : MonoBehaviour
{
    public List<Observer> observers;
    public string triggerType;
    /// <summary>
    /// simple value box (types tho are not simple at all)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    class simpleValue<T>
    {
        public T value;
    }

    /// <summary>
    /// MemberInfo box, combines Fields and Properties Info.
    /// This one might let us use Unity Components as native data sources.
    /// </summary>
    class memberBox
    {
        /// <summary>
        /// Note our state internally to distinguish unboxing procedure for FieldInfo and PropertyInfo.
        /// </summary>
        protected bool isField = true;
        /// <summary>
        /// FieldInfo boxed member.
        /// </summary>
        private FieldInfo _member_f;
        /// <summary>
        /// PropertyInfo boxed member.
        /// </summary>
        private PropertyInfo _member_p;
        /// <summary>
        /// FieldInfo boxing constructor.
        /// </summary>
        /// <param name="f"></param>
        public memberBox(FieldInfo f)
        {
            isField = true;
            _member_f = f;
        }
        /// <summary>
        /// PropertyInfo boxing constructor.
        /// </summary>
        /// <param name="p"></param>
        public memberBox(PropertyInfo p)
        {
            isField = false;
            _member_p = p;
        }
        /// <summary>
        /// MemberInfo boxing constructor.
        /// </summary>
        /// <param name="m"></param>
        public memberBox(MemberInfo m)
        {
            if (m.MemberType == MemberTypes.Field) {
                isField = true;
                _member_f = (FieldInfo)m;
            } else if (m.MemberType == MemberTypes.Property)
            {
                isField = false;
                _member_p = (PropertyInfo)m;
            } else
            {
                throw new Exception("member is not a field or property");
            }
        }
        /// <summary>
        /// Unboxing Value of the boxed member.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public object GetValue(object instance)
        {
            if (isField)
                return _member_f.GetValue(instance);
            else
                return _member_p.GetValue(instance, null);
        }
        /// <summary>
        /// Unboxing Type of the boxed member.
        /// </summary>
        public Type MemberType
        {
            get
            {
                if (isField)
                    return _member_f.FieldType;
                else
                    return _member_p.PropertyType;
            }
        }
    }

    public string achievementId;

    public bool status;
    public bool ready = false;

    List<UnityEngine.Component> ComponentObject;
    List<string> ComponentField;
    List<string> ComponentGoal;
    List<MatchingMechanism> ComponentMatchMechanism;

    private Type componentType;
    private Type observableType;
    //TODO: Check if memberBox is suitable here
    private memberBox observableField;
    //TODO: Check where this one came from, delete if useless
    private MemberInfo observableGoal;

    // Use this for initialization
    void Start()
    {
        observers = new List<Observer>();
        Trigger trigger = (Trigger) transform.gameObject.AddComponent(Type.GetType(triggerType));
        trigger.achievementObserver = this;
        setObservers();
    }

    /// <summary>
    /// set up observers on each field that achievement components require
    /// </summary>
    void setObservers()
    {
        bool asProperty = false;
        //components we are observing
        ComponentObject = new List<UnityEngine.Component>();
        //fields we are observing
        ComponentField = new List<string>();
        //goal we are trying to reach
        ComponentGoal = new List<string>();
        //matching mechanism for goal reaching
        ComponentMatchMechanism = new List<MatchingMechanism>();
        //read achievement that corresponds to our id
        Achievement achievement = GameObject.FindObjectOfType<AchievementManager>().Instance.ReadAchievement(achievementId);
        //for each achievement component - find appropriate monobehaviour and map it
        for (int i = 0; i < achievement.acomponents.Count; i++)
        {
            if (achievement.acomponents[i].GameObjectTag != "Untagged")
            {
                GameObject[] gos = GameObject.FindGameObjectsWithTag(achievement.acomponents[i].GameObjectTag);
                GameObject go = gos.Where(g => g.name.Contains(achievement.acomponents[i].GameObjectName)).FirstOrDefault<GameObject>();
                UnityEngine.Component mb = go.GetComponent(achievement.acomponents[i].MonoBehaviourName);
                ComponentObject.Add(mb);
            }
            else
            {
                GameObject go = GameObject.Find(achievement.acomponents[i].GameObjectName);
                UnityEngine.Component mb = go.GetComponent(achievement.acomponents[i].MonoBehaviourName);
                ComponentObject.Add(mb);
            }
            ComponentField.Add(achievement.acomponents[i].TargetFieldName);
            ComponentGoal.Add(achievement.acomponents[i].TargetValue);
            ComponentMatchMechanism.Add(achievement.acomponents[i].MatchingMechanism);
        }

        //now setup all the observers
        for (int i = 0; i < ComponentObject.Count; i++)
        {
            //type of the component (MonoBehaviour) we are going to observe
            componentType = ComponentObject[i].GetType();
            //member we are going to observe
            observableField = new memberBox(componentType.GetMember(ComponentField[i])[0]);

            //type of the member we are going to observe
            observableType = observableField.MemberType;
            //our base observer
            Observer obs;
            //if observable member has value type - try this
            if (observableType.IsValueType) {
                //create generic struct observer
                object genericObs = Activator.CreateInstance(typeof(StructGenericObserver<>).MakeGenericType(observableType));
                //prepare type converter
                TypeConverter converter = TypeDescriptor.GetConverter(observableType);
                //if we can parse a value from it's string representation - go on
                if (converter.IsValid(ComponentGoal[i]) && converter.CanConvertFrom(typeof(string))) {
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetMethod("setgoal").Invoke(genericObs, new object[] { converter.ConvertFromString(ComponentGoal[i]) });
                //else try to convert it by json string parsing
                } else {
                    //find generic box type
                    Type gsvT = typeof(simpleValue<>).MakeGenericType(observableType);
                    //instantiate generic box
                    var simpleValueBox = Activator.CreateInstance(gsvT);
                    //convert json to box
                    JsonUtility.FromJsonOverwrite("{ \"value\" : " + ComponentGoal[i].Replace("'","\"") + " } ", simpleValueBox);
                    //unbox the value
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetMethod("setgoal").Invoke(
                        genericObs, new object[] {
                            gsvT.GetField("value").GetValue(simpleValueBox)
                        }
                    );
                }

                //catch unity's structures that we cant extend by default and ask appropriate matchers for them
                if (observableType.ToString().Equals("UnityEngine.Vector3"))
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetField("matcher").SetValue(genericObs, MatcherHelper.MatcherVector3(ComponentMatchMechanism[i]));
                else if (observableType.ToString().Equals("UnityEngine.Quaternion"))
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetField("matcher").SetValue(genericObs, MatcherHelper.MatcherQuaternion(ComponentMatchMechanism[i]));
                else if (observableType.ToString().Equals("UnityEngine.Color"))
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetField("matcher").SetValue(genericObs, MatcherHelper.MatcherColor(ComponentMatchMechanism[i]));
                else
                //for other structs - ask our matcher helper to provide prepared matcher
                    typeof(StructGenericObserver<>).MakeGenericType(observableType).GetField("matcher").SetValue(genericObs, typeof(MatcherHelper).GetMethod("getMatcher").MakeGenericMethod(observableType).Invoke(null, new object[] { ComponentMatchMechanism[i] }));
                //tie our generic struct observer to base observer
                obs = (Observer) genericObs;
            //if observable member has reference type - use that
            }
            else {
                //create generic class observer
                var genericObs = Activator.CreateInstance(typeof(ClassGenericObserver<>).MakeGenericType(observableType));
                //prepare type converter
                TypeConverter converter = TypeDescriptor.GetConverter(observableType);
                //if we can parse a value from it's string representation - go on
                if (converter.IsValid(ComponentGoal[i]) && converter.CanConvertFrom(typeof(string)))
                {
                    typeof(ClassGenericObserver<>).MakeGenericType(observableType).GetMethod("setgoal").Invoke(genericObs, new object[] { converter.ConvertFromString(ComponentGoal[i]) });
                }
                //else try to convert it by type switching
                else {
                    typeof(ClassGenericObserver<>).MakeGenericType(observableType).GetMethod("setgoal").Invoke(genericObs, new object[] { Convert.ChangeType(ComponentGoal[i], observableType) });
                }
                //ask our matcher helper to extract appropriate matcher from the class definition
                typeof(ClassGenericObserver<>).MakeGenericType(observableType).GetField("matcher").SetValue( genericObs, typeof(MatcherHelper).GetMethod("getMatcher").MakeGenericMethod(observableType).Invoke(null, new object[] { ComponentMatchMechanism[i] }));
                //tie our generic class observer to base observer
                obs = (Observer)genericObs;
            }
            //set initial value for the observer
            obs.setval(observableField.GetValue(ComponentObject[i]));
            //store observer in the list
            observers.Add(obs);
        }
        //all observers are ready, it is safe to start observation
        ready = true;
    }

    //for each fixed update 
    void FixedUpdate()
    {
        //if we are ready
        if (ready)
        {
            //cycle through the list of components we are observing
            for (int i = 0; i < ComponentObject.Count; i++)
            {
                //get MonoBehaviour type
                componentType = ComponentObject[i].GetType();
                //get observable member
                observableField = new memberBox(componentType.GetMember(ComponentField[i])[0]);
                //set member value to the observer value 
                //after this, observer will hook-up new value and check it with the goal using it's matcher
                observers[i].setval(observableField.GetValue(ComponentObject[i]));
            }
            //if all observers reached their goal, achievement is unlocked. Set our status to true and let the trigger to fire
            status = observers.All<Observer>(o => o.status == true);
        }
    }

}
