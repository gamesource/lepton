﻿using UnityEngine;

//TODO: Split base Trigger from Lepton Trigger and let developers inherit from them to implement custom triggers
public class Trigger : MonoBehaviour {
    public delegate void AchievementEvent (string id);
    public AchievementEvent Unlock;
    public AchievementObserver achievementObserver;

    public void Register()
    {
        //find achievement manager and register this trigger
        GameObject.FindObjectOfType<AchievementManager>().Instance.RegisterTrigger(this);
    }
}
