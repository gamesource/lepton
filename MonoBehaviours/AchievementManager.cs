﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

[DisallowMultipleComponent]
public class AchievementManager : MonoBehaviour {

    public enum AchievementDescriptorSource
    {
        resource,
        www
    }

    #region configuration
    public AchievementDescriptorSource DescriptorSource;
    public string DescriptorName;
    public string DescriptorUrl;
    #endregion

    #region protected empty constructor
    protected AchievementManager() { }
    #endregion

    #region subscribers
    public delegate void UiEvent(Achievement achievement);
    public UiEvent UiAchievementUnlocked;
    #endregion

    #region members
    protected GameObject _observersContainer;
    private AchievementsDescriptor descriptor;
    protected AchievementManager _instance;
    protected List<Achievement> _achievements;
    public List<Trigger> triggers;
    public AchievementRegisterer registerer;
    #endregion

    #region lazy construction
    public AchievementManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = gameObject.AddComponent<AchievementManager>();
            return _instance;
        }
        protected set
        {
            Debug.LogError("should not set instance of AchievementManager directly");
        }
    }
    #endregion

    void Awake()
    {
        //mark self with the appropriate tag
        transform.gameObject.tag = "AchievementManager";
        //if new scene has achievement manager - destroy it
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("AchievementManager"))
        {
            if (go != transform.gameObject) Destroy(transform.gameObject);
        }
        //if this is not a first achievement manager - destroy it
        if (_instance != null && _instance != this)
        {
            Debug.LogError("instance of AchievementManager exists");
            Destroy(this);
            throw new System.Exception("instance of AchievementManager exists");
        }
        //if we are legitimated achievement manager - persist us between scenes
        else
        {
            _instance = this;
            DontDestroyOnLoad(transform.gameObject);
            if (_achievements == null)
            {
                descriptor = new AchievementsDescriptor();
                //load achievements descriptor
                switch (DescriptorSource) {
                    case AchievementDescriptorSource.resource: { descriptor = JsonUtility.FromJson<AchievementsDescriptor>(Resources.Load<TextAsset>(DescriptorUrl + DescriptorName).text); break; }
                    case AchievementDescriptorSource.www: { descriptor = JsonUtility.FromJson<AchievementsDescriptor>(WWW.LoadFromCacheOrDownload(DescriptorUrl + DescriptorName,0).text); break; }
                }
                _achievements = new List<Achievement>();
                _achievements.AddRange(descriptor.achievements.AsEnumerable().Where(a => a.scene == UnityEngine.SceneManagement.SceneManager.GetActiveScene().name));
            }
        }

        //Instantiate temporal achievement registerer (provides vortual methods, so one can inherit and wrap inside its own registerer)
        registerer = (AchievementRegisterer) Activator.CreateInstance(Type.GetType(descriptor.registerer != null ? descriptor.registerer : "AchievementRegisterer"), new object[] { Action.CreateDelegate(typeof(Action<bool>), this, "InstantiateObservers") });  
    }
    
    /// <summary>
    /// When the next Scene is loaded, previous observers should be discarded, and the new ones instantiated instead.
    /// </summary>
    void OnLevelWasLoaded()
    {
        InstantiateObservers(true);
    }

    /// <summary>
    /// Callback to ensure that we instantiate observers only after Registerer is connected to it's backend.
    /// </summary>
    /// <param name="value"></param>
    void InstantiateObservers(bool value)
    {
        if (value)
        {
            //filter appropriate achievements by scene
            _achievements.AddRange(descriptor.achievements.AsEnumerable().Where(a => a.scene == UnityEngine.SceneManagement.SceneManager.GetActiveScene().name));
            //Prepare container for Lepton components
            _observersContainer = new GameObject("Observers Container");
            foreach (Achievement _a in _achievements)
            {
                AchievementCheck(_a.id);
            }
        } else
        {
            Debug.LogError("Instantiating Observers when Registerer isn't ready is omitted");
        }
    }

    /// <summary>
    /// Register new trigger
    /// </summary>
    /// <param name="trigger"></param>
    public void RegisterTrigger(Trigger trigger)
    {
        trigger.Unlock += AchievementUnlockEventHandler;
        if (triggers == null)
            triggers = new List<Trigger>();
        triggers = triggers.Where(item => item != null).ToList();
        triggers.Add(trigger);
    }

    /// <summary>
    /// Read achievement by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public Achievement ReadAchievement(string id)
    {
        return _achievements.Where(achievement => achievement.id == id).Single<Achievement>();
    }

    /// <summary>
    /// Achievement Unlocking Procedure
    /// Handler, that is associated with each registered trigger. Once trigger fires, it also calls this handler.
    /// Trigger provides string identifier of the achievement, so we can use it to unlock achievement in the service.
    /// </summary>
    /// <param name="id"></param>
    public void AchievementUnlockEventHandler(string id)
    {
        StartCoroutine(registerer.UnlockAchievementAsync(id, AchievementUnlockEventHandlerCallback)); 
    }
    public void AchievementUnlockEventHandlerCallback(bool result, string id)
    {
        if (result)
        {
            Debug.LogWarning("Achievement " + id + " unlocked");
            if (UiAchievementUnlocked != null)
                UiAchievementUnlocked(ReadAchievement(id));
        }
        else
            Debug.LogWarning("Achievement " + id + " locked");
    }

    /// <summary>
    /// Achievement Checking procedure. 
    /// We Check each achievement before instantiating observers, because we need no observers for yet achieved achievements. 
    /// That way we save some performance.
    /// </summary>
    /// <param name="id"></param>
    public void AchievementCheck(string id)
    {
        StartCoroutine(registerer.CheckAchievementAsync(id, AchievementCheckCallback));
    }
    public void AchievementCheckCallback(bool result, string id)
    {
        if (!result)
        {
            GameObject g = new GameObject(id);
            AchievementObserver ao = g.AddComponent<AchievementObserver>();
            ao.triggerType = ReadAchievement(id).trigger != null ? ReadAchievement(id).trigger : "LeptonTrigger";
            ao.achievementId = id;
            g.transform.parent = _observersContainer.transform;
            Debug.LogWarning("Achievement " + id + " is locked, observe it");
        }
        else
            Debug.LogWarning("Achievement " + id + " already unlocked");
    }
}
