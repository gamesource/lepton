﻿using System;
using System.Collections;
using UnityEngine;

public class AchievementRegisterer : ILeptonUnlockable, ILeptonCheckable, ILeptonLockable, ILeptonResettable
{
    /// <summary>
    /// Constructor, use it to prepare our registerer (connect to backend, authenticate, etc.)
    /// readyCallback is called once registerer is ready
    /// </summary>
    /// <param name="readyCallback"></param>
    public AchievementRegisterer(Action<bool> readyCallback)
    {
        //instant ready because we use no backend, only local storage
        if (readyCallback!=null)
        readyCallback(true);
    }

    /// <summary>
    /// Check achievement by its id.
    /// true corresponds to unlocked achievement
    /// false corresponds to locked achievement
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool CheckAchievement(string id)
    {
        if (PlayerPrefs.HasKey(id))
            if (PlayerPrefs.GetInt(id) == 1)
                return true;
        return false;
    }

    /// <summary>
    /// Locks achievement, so it can be unlocked further
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool LockAchievement(string id)
    {
        PlayerPrefs.DeleteKey(id);
        return true;
    }

    /// <summary>
    /// Unlocks achievement
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public virtual bool UnlockAchievement(string id)
    {
        PlayerPrefs.SetInt(id, 1);
        return true;
    }

    /// <summary>
    /// Resets all achievements state.
    /// Useful for wipe.
    /// </summary>
    /// <returns></returns>
    public virtual bool ResetAchievements()
    {
        PlayerPrefs.DeleteAll();
        return true;
    }

    /// <summary>
    /// By default, Async realization calls synchronous reqlization, so one can implement synchronous only. 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public virtual IEnumerator CheckAchievementAsync(string id, Action<bool, string> callback)
    {
        callback(CheckAchievement(id), id);
        yield return true;
    }
    /// <summary>
    /// By default, Async realization calls synchronous reqlization, so one can implement synchronous only. 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public virtual IEnumerator LockAchievementAsync(string id, Action<bool, string> callback)
    {
        callback(LockAchievement(id), id);
        yield return true;
    }

    /// <summary>
    /// By default, Async realization calls synchronous reqlization, so one can implement synchronous only. 
    /// </summary>
    /// <param name="id"></param>
    /// <param name="callback"></param>
    /// <returns></returns>
    public virtual IEnumerator UnlockAchievementAsync(string id, Action<bool, string> callback)
    {
        callback(UnlockAchievement(id), id);
        yield return true;
    }

    /// <summary>
    /// By default, Async realization calls synchronous reqlization, so one can implement synchronous only. 
    /// </summary>
    /// <param name="callback"></param>
    /// <returns></returns>
    public virtual IEnumerator ResetAchievementsAsync(Action<bool> callback)
    {
        callback(ResetAchievements());
        yield return true;
    }
}
