﻿public interface ILeptonUnlockable
{
    bool UnlockAchievement(string id);
    System.Collections.IEnumerator UnlockAchievementAsync(string id, System.Action<bool,string> callback);
}
