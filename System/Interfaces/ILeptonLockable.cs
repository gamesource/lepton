﻿public interface ILeptonLockable
{
    bool LockAchievement(string id);
    System.Collections.IEnumerator LockAchievementAsync(string id, System.Action<bool,string> callback);
}