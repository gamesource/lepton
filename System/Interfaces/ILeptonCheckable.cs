﻿public interface ILeptonCheckable
{
    bool CheckAchievement(string id);
    System.Collections.IEnumerator CheckAchievementAsync(string id, System.Action<bool,string> callback);
}
