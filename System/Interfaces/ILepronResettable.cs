﻿public interface ILeptonResettable
{
    bool ResetAchievements();
    System.Collections.IEnumerator ResetAchievementsAsync(System.Action<bool> callback);
}