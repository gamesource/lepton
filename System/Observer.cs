﻿public abstract class Observer
{
    #region members
    /// <summary>
    /// status of this observer
    /// </summary>
    public bool status { get; set; }
    #endregion
    #region virtual methods
    /// <summary>
    /// vurtual check method, returns status by default. You may override it or use as is
    /// </summary>
    /// <returns>bool status</returns>
    public virtual bool check()
    {
        return status;
    }
    #endregion
    #region abstract methods
    /// <summary>
    /// getter for value of this observer
    /// </summary>
    /// <returns>object value</returns>
    public abstract object getval();
    /// <summary>
    /// setter for value of this observer
    /// </summary>
    /// <param name="val"></param>
    public abstract void setval(object val);
    /// <summary>
    /// getter for goal of this observer
    /// </summary>
    /// <returns>object goal</returns>
    public abstract object getgoal();
    /// <summary>
    /// setter for goal of this observer
    /// </summary>
    /// <param name="goal"></param>
    public abstract void setgoal(object goal);
    #endregion
}
