﻿using System.Collections.Generic;

[System.Serializable]
public class Achievement {
    public string id;
    public string scene;
    public string trigger;
    public List<AchievementComponent> acomponents;
}
