﻿[System.Serializable]
public class AchievementComponent
{
    public string GameObjectTag;
    public string GameObjectName;
    public string MonoBehaviourName;
    public string TargetFieldName;
    public string TargetValue;
    public MatchingMechanism MatchingMechanism;
}
