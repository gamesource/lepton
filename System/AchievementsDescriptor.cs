﻿using System.Collections.Generic;

[System.Serializable]
public class AchievementsDescriptor {
    public string registerer;
    public List<Achievement> achievements;
}
