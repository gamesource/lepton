﻿using System;

public class StructGenericObserver<T> : Observer where T : struct
{
    public T goal;
    protected T _val;
    public T val
    {
        get
        {
            return _val;
        }

        set
        {
            _val = value;
            status = check();
        }
    }

    public Func<T, T, bool> matcher;

    public override bool check()
    {
        return matcher(_val, goal);
    }

    public override object getval()
    {
        return val;
    }
    public override void setval(object newval)
    {
        val = (T)newval;
    }

    public override object getgoal()
    {
        return goal;
    }
    public override void setgoal(object newgoal)
    {
        goal = (T)newgoal;
    }

}
