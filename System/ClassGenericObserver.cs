﻿using System;

public class ClassGenericObserver<T> : Observer where T : class
{
    public T goal;
    protected T _val;
    public T val
    {
        get
        {
            return _val;
        }

        set
        {
            _val = value;
            status = check();
        }
    }

    public Func<T, T, bool> matcher;

    public override bool check()
    {
        return matcher(_val, goal);
    }

    public override object getval()
    {
        return val;
    }
    public override void setval(object newval)
    {
        val = newval as T;
    }

    public override object getgoal()
    {
        return goal;
    }
    public override void setgoal(object newgoal)
    {
        goal = newgoal as T;
    }

}
