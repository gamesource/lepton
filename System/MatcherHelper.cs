﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using UnityEngine;

public static class MatcherHelper
{
    public static Func<T, T, bool> getMatcher<T>(MatchingMechanism mechanism)
    {
        TypeCode tc = Type.GetTypeCode(typeof(T));
        switch (tc) {
            case TypeCode.Boolean:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
                {
                    switch (mechanism)
                    {
                        case MatchingMechanism.equal:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == 0; };
                                break;
                            }
                        case MatchingMechanism.equivalent:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == 0; };
                                break;
                            }
                        case MatchingMechanism.greater:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) > 0; };
                                break;
                            }
                        case MatchingMechanism.less:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) < 0; };
                                break;
                            }
                    }
                    return null;
                    break;
                }
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
            case TypeCode.Decimal:
            case TypeCode.Single:
                {
                    switch (mechanism)
                    {
                        case MatchingMechanism.equal:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == 0; };
                                break;
                            }
                        case MatchingMechanism.equivalent:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == -(another as IComparable).CompareTo(one); };
                                break;
                            }
                        case MatchingMechanism.greater:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) > 0; };
                                break;
                            }
                        case MatchingMechanism.less:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) < 0; };
                                break;
                            }
                    }
                    return null;
                    break;
                }
            case TypeCode.Char:
                {
                    switch (mechanism)
                    {
                        case MatchingMechanism.equal:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == 0; };
                                break;
                            }
                        case MatchingMechanism.equivalent:
                            {
                                return (one, another) => { return char.ToUpperInvariant(Convert.ToChar(one)) == char.ToUpperInvariant(Convert.ToChar(another)); };
                                break;
                            }
                        case MatchingMechanism.greater:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) > 0; };
                                break;
                            }
                        case MatchingMechanism.less:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) < 0; };
                                break;
                            }
                    }
                    return null;
                    break;
                }
            case TypeCode.String:
                {
                    switch (mechanism)
                    {
                        case MatchingMechanism.equal:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) == 0; };
                                break;
                            }
                        case MatchingMechanism.equivalent:
                            {
                                return (one, another) => { return string.Compare(Convert.ToString(one), Convert.ToString(another), true) == 0; };
                                break;
                            }
                        case MatchingMechanism.greater:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) > 0; };
                                break;
                            }
                        case MatchingMechanism.less:
                            {
                                return (one, another) => { return (one as IComparable).CompareTo(another) < 0; };
                                break;
                            }
                    }
                    return null;
                    break;
                }
            default:
                {
                    switch (mechanism)
                    {
                        case MatchingMechanism.equal:
                            {
                                var one = Expression.Parameter(typeof(T), "one");
                                var another = Expression.Parameter(typeof(T), "another");
                                var method = typeof(T).GetMethod("Equal", new[] { typeof(T), typeof(T) });
                                return Expression.Lambda<Func<T, T, bool>>(Expression.Call(method, one, another), one, another).Compile();
                                break;
                            }
                        case MatchingMechanism.equivalent:
                            {
                                var one = Expression.Parameter(typeof(T), "one");
                                var another = Expression.Parameter(typeof(T), "another");
                                var method = typeof(T).GetMethod("Equivalent", new[] { typeof(T), typeof(T) });
                                return Expression.Lambda<Func<T, T, bool>>(Expression.Call(method, one, another), one, another).Compile();
                                break;
                            }
                        case MatchingMechanism.greater:
                            {
                                var one = Expression.Parameter(typeof(T), "one");
                                var another = Expression.Parameter(typeof(T), "another");
                                var method = typeof(T).GetMethod("Greater", new[] { typeof(T), typeof(T) });
                                return Expression.Lambda<Func<T, T, bool>>(Expression.Call(method, one, another), one, another).Compile();
                                break;
                            }
                        case MatchingMechanism.less:
                            {
                                var one = Expression.Parameter(typeof(T), "one");
                                var another = Expression.Parameter(typeof(T), "another");
                                var method = typeof(T).GetMethod("Less", new[] { typeof(T), typeof(T) });
                                return Expression.Lambda<Func<T, T, bool>>(Expression.Call(method, one, another), one, another).Compile();
                                break;
                            }
                    }
                    return null;
                    break;
                }
        }
    }

    public static Func<Vector3, Vector3, bool> MatcherVector3(MatchingMechanism mechanism)
    {
        switch (mechanism)
        {
            case MatchingMechanism.equal:
                {
                    return (one, another) => 
                    {
                        return Mathf.Approximately(one.x, another.x) && Mathf.Approximately(one.y, another.y) && Mathf.Approximately(one.z, another.z);
                    };
                    break;
                }
            case MatchingMechanism.equivalent:
                {
                    return (one, another) => 
                    {
                        return Mathf.Approximately(one.sqrMagnitude, another.sqrMagnitude);
                    };
                    break;
                }
            case MatchingMechanism.greater:
                {
                    return (one, another) =>
                    {
                        return one.sqrMagnitude > another.sqrMagnitude;
                    };
                    break;
                }
            case MatchingMechanism.less:
                {
                    return (one, another) =>
                    {
                        return one.sqrMagnitude < another.sqrMagnitude;
                    };
                    break;
                }
            default:
                {
                    return (one, another) =>
                    {
                        return Mathf.Approximately(one.sqrMagnitude, another.sqrMagnitude);
                    };
                    break;
                }
        }
    }

    public static Func<Quaternion, Quaternion, bool> MatcherQuaternion(MatchingMechanism mechanism)
    {
        switch (mechanism)
        {
            case MatchingMechanism.equal:
                {
                    return (one, another) =>
                    {
                        return Mathf.Approximately(Quaternion.Angle(one, another), 0f);
                    };
                    break;
                }
            case MatchingMechanism.equivalent:
                {
                    return (one, another) =>
                    {
                        return Mathf.Approximately(Quaternion.Angle(one,another), 0f) || Mathf.Approximately(Quaternion.Angle(one, another), 180f);
                    };
                    break;
                }
            case MatchingMechanism.greater:
                {
                    return (one, another) =>
                    {
                        return Quaternion.Angle(one, Quaternion.identity) > Quaternion.Angle(another, Quaternion.identity);
                    };
                    break;
                }
            case MatchingMechanism.less:
                {
                    return (one, another) =>
                    {
                        return Quaternion.Angle(one, Quaternion.identity) < Quaternion.Angle(another, Quaternion.identity);
                    };
                    break;
                }
            default:
                {
                    return (one, another) =>
                    {
                        return Mathf.Approximately(Quaternion.Angle(one, another), 0f);
                    };
                    break;
                }
        }
    }

    public static Func<Color, Color, bool> MatcherColor(MatchingMechanism mechanism)
    {
        switch (mechanism)
        {
            case MatchingMechanism.equal:
                {
                    return (one, another) =>
                    {
                        return one.Equals(another);
                    };
                    break;
                }
            case MatchingMechanism.equivalent:
                {
                    return (one, another) =>
                    {
                        return Mathf.Approximately(one.grayscale, another.grayscale);
                    };
                    break;
                }
            case MatchingMechanism.greater:
                {
                    return (one, another) =>
                    {
                        return one.grayscale > another.grayscale;
                    };
                    break;
                }
            case MatchingMechanism.less:
                {
                    return (one, another) =>
                    {
                        return one.grayscale < another.grayscale;
                    };
                    break;
                }
            default:
                {
                    return (one, another) =>
                    {
                        return one.Equals(another);
                    };
                    break;
                }
        }
    }

}