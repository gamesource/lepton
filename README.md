# lepton
## content
> - [about](#about)
> - [how to use](#how-to-use)
>   - [descriptors](#descriptors)
>   - [custom types](#custom-types)
> - [tech](#tech)
> - [integration](#integration)
> - [extending](#extending)
> - [restrictions](#restrictions)
> - [disclaimer](#disclaimer)

## about
"Lepton" is an open-source achievement system for Unity3d game engine.  
The main pros of this system is it's dynamic-approach. Achievements are described as an explicit descriptors, passed into your game either from local storage or from the internet. When such descriptor is received by the system, it deploys all the things on runtime, allowing you to add, remove and change any achievement in the game with no need to rebuild it.

## how to use
### descriptors
Lepton system uses Achievements Descriptor (further called as AcD) to define all the achievements that is unlockable in your game. Common AD is just a json file, stored either in local storage or in the internet.  
Hopefully, we will provide you with the backend service to manage AcDs, otherwise you should host json file by yourself.  
To form a valid AcD you can use next pattern:
```json
{
  "registerer" : <type of the registerer> [optional parameter],
  "achievements" : [
    {
      "id" : <string>,
      "scene" : <string, name of the scene>,
      "trigger" : <string, type of the trigger> [optional parameter],
      "acomponents" : [
        {
          "GameObjectName" : <string, name of the game object>,
          "GameObjectTag" : <string, tag of the game object>,
          "MonoBehaviourName" : <string, name of the component>,
          "TargetFieldName" : <string, name of the field to observe>,
          "TargetValue" : <string, target value>,
          "MatchingMechanism" : <int, matching mechanism enum>
        },
        {
          "GameObjectName" : <string, name of the game object>,
          "GameObjectTag" : <string, tag of the game object>,
          "MonoBehaviourName" : <string, name of the component>,
          "TargetFieldName" : <string, name of the field to observe>,
          "TargetValue" : <string, target value>,
          "MatchingMechanism" : <int, matching mechanism enum>
        },
      ]
    },
    {
      "id" : <string>,
      "scene" : <string, name of the scene>,
      "trigger" : <string, type of the trigger> [optional parameter],
      "acomponents" : [
        {
          "GameObjectName" : <string, name of the game object>,
          "GameObjectTag" : <string, tag of the game object>,
          "MonoBehaviourName" : <string, name of the component>,
          "TargetFieldName" : <string, name of the field to observe>,
          "TargetValue" : <string, target value>,
          "MatchingMechanism" : <int, matching mechanism enum>
        }
      ]
    }
  ]
}
```
Here:  
`<string> registerer` is an optional type name of the registerer.
`<array> achievements` is an array of all achievements, current descriptor describes.  
`<string> id` is an unique identifier of your achievement, that match the identifier of your
 achievement in third party achievement system (like GooglePlay or Steam).  
`<string> scene` is a name of the scene in your game, where this achievement is unlockable.  
`<string> trigger` is an optional type name of the trigger.
`<array> acomponents` is an array of achievement components. Note, that achievement unlocks only when all of its component's goals are reached.  
`<string> GameObjectName` is a name of the unity GameObject on the Scene.  
`<string> GameObjectTag` is a tag of the unity Gameobject. Use it when it helps to identify the game object on the scene. To ommit the tag, use `"Untagged"` value.  
`<string> MonoBehaviourName` is a name of the component (most common - MonoBehaviour), attached to the GameObject.  
`<string> TargetFieldName` is a name of the field inside component, which we want to observe.  
`<string> TargetValue` is a string value, convertible to the value of the same type as the target field we are observing.  
`<string> MatchingMechanism` is a matching mechanism type we want to use on this achievement component. It uses next enum:
```csharp
enum MatchingMechanism {
    less = 0,
    greater = 1,
    equal = 2,
    equivalent = 3
}
```
### custom types
If you want to use a custom type in the lepton system, first thing you should think of is a JSON serialization/deserialization. Generally, all you need to do is to annotate your class with the `[System.Serializable]` attribute.  
The thing is your class JSON representation must be placed into the `TargetValue` field of the achievement component in the AcD.  
If your custom type is simple enough, you can provide it with the converter from string type, it is also valid.
Then you should implement all matching mechanisms lepton supports. By now, it requires four of them: less than, greater than, equal with, equivalent with. All of these may be found in the `IMatchable` interface declaration.  
Please, be patient, as we are planning to refactor current support of custom types, especially in the Achievement Descriptor's side.  
To learn more about introducing a custom types, follow to the [extending](#extending) chapter.

---
## tech
Under the hood lepton uses a lot of reflection, to provide you with the ability to define achievement unlock condition based on MonoBehaviours in the scene.  
You can define achievement as a set of components, all of which should have a simple goal to reach. Goals are such values and comparing rules, which allows you to track any parameter of any MonoBehaviour on the scene.  
>As long as MonoBehavious should be attached to the GameObjects, to find particular one you generally provide a path to it. Path to the parameter you wish to observe consists of four components:
- Scene name
- GameObject name (optionally with tag)
- MonoBehaviour name
- Field name

```csharp
Scene.GameObject.MonoBehaviour.Field
```
After you defined a path to the field you want to observe, you generally want to choose a goal value, and a matching mechanism.  
>Goal value is a value of a simple type, or any type that is compatible with Lepton (more of this later).

Matching mechanism may be one of these:
- less than
- greater than
- equal with
- equivalent with

Less than - means that the parameter which you are observing at a particular time should be less than target value. As soon as observed parameter is less than target value, achievement's component goal is reached.

Greater than - means that the parameter which you are observing at a particular time should be greater than target value. As soon as observed parameter is greater than target value, achievement's component goal is reached.

Equal with - means that the parameter which you are observing at a particular time should be equal with the target value. As soon as observed parameter is equal with target value, achievement's component goal is reached.

Equivalent with - this one is a special case of euqality, but as far as equal only looks to direct equality, equivalent may be used in the case when parameter could be processed not directly, but after some processing.

> for example, two Vector2 values V1 and V2 are equal, when   
``` V1 = V2 ```  
> two Vector2 values V1 and V2 are equivalent, when  
``` |V1| = |V2| ```  
> in this particular case, vectors with the same magnitude but different orientation may be considered equivalent.  

> another example, two string values S1 and S2 are equal, when   
``` S1 = S2 *case aware* ```  
> two string values S1 and S2 are equivalent, when  
``` S1 = S2 *ignore case* ```  
> in this particular case, "lepton" is not equal to "leptON", but "lepton" is equivalent to "leptON".  

## integration
Lepton system is aimed on goal-detection. This means that with Lepton in its game, one can simply register such combination of different values, that leads to achievement unlocking.  
In general, when each achievement's component reached its goal, corresponding trigger is fired. Those triggers are then handled by achievements manager and routed directly to the achievement system you use (e.g. GooglePlay, Steam). Those third-party achievement systems are wrapped around with adapters, so if there is a need in cross-platform deployment on different achievement systems, just plug appropriate one into the adapter.  
> Previous means that one can also write it's own backend solution for the achievement system and plug it into Lepton. If this is a case, contact us, and we will include it in the distributive package.

## extending
Lepton introduces out-of-the-box support for simple data types used in c# (such as int, float, decimal, string, bool) and some Unity3D structures (Vector2, Vector3, Quaternion).  
If you want to introduce your own type to the Lepton, make it in the following manner:
```csharp
[System.Serializable]
class MyLeptonType implements IMatchable {
    #region Own
    /**
    * here goes your fields and properties
    * and your methods could also be here
    */
    #endregion

    #region LeptonSupport
    public bool Lesser (MyLeptonType this, MyLeptonType other){
    }

    public bool Greater(MyLeptonType this, MyLeptonType other){
    }

    public bool Equal(MyLeptonType this, MyLeptonType other){
    }

    public bool Equivalent(MyLeptonType this, MyLeptonType other){
    }
    #endregion
}
```
Note, that your class should be annotated with System.Serializable attribute and contain four matching methods, corresponding to matchers in lepton. Names of those methods must have the same names. Those methods are declared as IMathcable interface, so feel free to use it.
```csharp
interface IMatchable {
    bool Lesser (T this, T other);
    bool Greater (T this, T other);
    bool Equal (T this, T other);
    bool Equivalent (T this, T other);
}
```

Beside this, Lepton can be explicitly extended in such way that is preferable for you.
First thing that you generally would extend is AchievementRegisterer.
AchievementRegisterer is a wrap around third part achievement backend systems. It converts signals from Lepton environment to the ones acceptable by particular achievement backend.
Achievement registerer implements four interfaces, which of them provides synchronous operation and asynchronous stub. Those interfaces are: ILeptonUnlockable, ILeptonCheckable, ILeptonLockable, ILeptonResettable.

```csharp
public interface ILeptonUnlockable
{
    bool UnlockAchievement(string id);
    System.Collections.IEnumerator UnlockAchievementAsync(string id, System.Action<bool,string> callback);
}
```

```csharp
public interface ILeptonCheckable
{
    bool CheckAchievement(string id);
    System.Collections.IEnumerator CheckAchievementAsync(string id, System.Action<bool,string> callback);
}
```

```csharp
public interface ILeptonLockable
{
    bool LockAchievement(string id);
    System.Collections.IEnumerator LockAchievementAsync(string id, System.Action<bool,string> callback);
}
```

```csharp
public interface ILeptonResettable
{
    bool ResetAchievements();
    System.Collections.IEnumerator ResetAchievementsAsync(System.Action<bool> callback);
}
```

AchievementRegisterer also has a crucial behaviour in its constructor.

```csharp
public AchievementRegisterer(Action<bool> readyCallback)
{
    //instant ready because we use no backend, only local storage
    if (readyCallback!=null)
    readyCallback(true);
}
```

readyCallback is a callback function, that notifies Lepton when AchievementRegisterer is ready. It mush be called with `true` parameter if registerer is ready to work, otherwise pass `false` and lepton will react properly.

>We recommend you to inherit your Registerers from AchievementRegisterer and override needed functionality.
>Lepton will load your Registerer by its TypeName based on data in AD.

Next thing one generally would like to extend is Trigger.
Trigger is responsible for generation achievement unlock event, which is further passed to UI system and AchievementRegisterer to consume.
Basic trigger component has a Register method, which registers current trigger in the system. Also, it has a method Unlock (which is wisely an event handler) which should be called with the achievement id parameter to unlock the achievement.
If you want to introduce your own trigger (maybe with special logic/magic you need), just inherit from the Trigger class. Then Call `Register()` method to register your trigger in the system. Once achievement should be unlocked - call `Unlock(<id>)` method.
Standard trigger in Lepton is `LeptonTrigger`

>As the conclusion: one may freely extend Lepton to suit specific situations by introducing own Types, Achievement Registerers and Triggers. Registerer is defined one per descriptor, hovever Triggers may be specific for each Achievement in the descriptor.

---
## restrictions
Currently Lepton uses JSON format to write Achievements Descriptor, so it is bound to unity's JsonUtility. To make Lepton work you need to deliver an AcD file to the client. By now, Lepton introduces only quantitative goals. Some qualitative goals, such as space dependent (vectors and quaternions) or string dependent (names) are supported, but may be considered as subject to refactor.

## disclaimer
Currently Lepton is in the developing stage, and it's architecture is a subject to change. We do not guarantee any backward-compatability. Third-party achievement systems support is not prioritized goal, so it will became an out-of-the-box feature in far future.
